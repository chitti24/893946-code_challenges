package com.cognizant.models;

public class Student {
	
	private int EnrollmentId;
	private int StudentId;
	private int CourseId;
//	private Course course;
	public int getEnrollmentId() {
		return EnrollmentId;
	}
	public void setEnrollmentId(int enrollmentId) {
		EnrollmentId = enrollmentId;
	}
	public int getStudentId() {
		return StudentId;
	}
	public void setStudentId(int studentId) {
		StudentId = studentId;
	}
	public int getCourseId() {
		return CourseId;
	}
	public void setCourseId(int courseId) {
		CourseId = courseId;
	}
	@Override
	public String toString() {
		return "Student [EnrollmentId=" + EnrollmentId + ", StudentId=" + StudentId + ", CourseId=" + CourseId + "]";
	}
	public Student() {
		super();
	}
	
}
