package com.cognizant.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.cognizant.models.Course;
@Component
public class CourseDaoImpl implements CourseDao {
	@Autowired
	private JdbcTemplate jdbctemplate;
	@Override
	public List<Course> getAllCourses() {
		// TODO Auto-generated method stub
		
		return jdbctemplate.query("select * from Course", new CourseRowMapper());
	}

	@Override
	public Course getCourseById(int CourseId) {
		// TODO Auto-generated method stub
		PreparedStatementSetter setter =new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				// TODO Auto-generated method stub
				ps.setInt(1, CourseId);
			}
		};
		return jdbctemplate.query("select * from Course where CourseId=?", setter, new ResultSetExtractor<Course>() {

			@Override
			public Course extractData(ResultSet rs) throws SQLException, DataAccessException {
				// TODO Auto-generated method stub
				Course course=null;
				if(rs.next()) {
					course=new Course();
					course.setCourseId(rs.getInt(1));
					course.setTitle(rs.getString(2));
					course.setFees(rs.getFloat(3));
					course.setDescription(rs.getString(4));
					course.setTrainer(rs.getString(5));
					course.setStart_Date(rs.getDate(6));
				}
				return course;
			}
		});
	}

}
