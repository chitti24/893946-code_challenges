package com.cognizant.dao;

import java.util.List;

import com.cognizant.models.Student;

public class StudentDTO {
	
	private List<Student> studentlist;

	public List<Student> getStudentlist() {
		return studentlist;
	}

	public void setStudentlist(List<Student> studentlist) {
		this.studentlist = studentlist;
	}
	
	
}
