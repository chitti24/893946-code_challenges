package com.restapi.mstock.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.restapi.mstock.models.CompanyDetails;

public class CompanyStockRowMapper implements RowMapper<CompanyDetails> {

	@Override
	public CompanyDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
		CompanyDetails details = new CompanyDetails();
		details.setCompanycode(rs.getInt(1));
		details.setCompanyname(rs.getString(2));
		details.setBriefdesc(rs.getString(3));
		details.setStockprice(rs.getDouble(4));
		
		return details;
	}

}
